# recursero-ciencia-abierta

Recursero con información para pensar una ciencia abierta y al servicio del pueblo, un sistema de propiedad intelectual y patentes que se ponga en función del bien común, y la articulación de todo esto mediante las instituciones.